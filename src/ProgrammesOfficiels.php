<?php


namespace Console;


class ProgrammesOfficiels
{

    private $aProgrammesOfficiels;
    private $fProgrammesOfficiels = "datas/programmes.json";
    private $questions = [];
    private $typeQuestionsChosen = [];

    const TQ1 = "Quels sont les themes de la matieres 'XXX' ?";
    const TQ2 = "Dans quelle(s) matiere(s) se trouve le theme 'XXX' ?";
    const TQ3 = "Quels sont les contenus du theme 'XXX' ?";
    const TQ4 = "Dans quelle matiere se trouve le contenu 'XXX' ?";
    const TQ5 = "Dans quel theme se trouve le contenu 'XXX' ?";
    const TQ6 = "Quelle(s) est(sont) la(les) capacité(s) attendue(s) dans le contenu 'XXX' ?";
    const TQ7 = "Quelle(s) est(sont) la(les) capacité(s) attendue(s) dans le theme 'XXX' ?";
    const TQ8 = "A quel contenu appartient la capacité 'XXX' ?";
    const TQ9 = "A quel theme appartient la capacité 'XXX' ?";
    const TQ10 = "Quel commentaire est annoté a la capacité 'XXX' ?";


    public function __construct()
    {
        $this->aProgrammesOfficiels = json_decode(file_get_contents($this->fProgrammesOfficiels), true);
    }

    public function getMatieres(){
        return \array_keys($this->getAProgrammesOfficiels());
    }

    public function getThemes($aMatiere){
        return $aMatiere["thèmes"];

    }

    public function removeMatiere(array $matieres){
        $po = $this->getAProgrammesOfficiels();

        foreach ($matieres as $matiere) {
            unset($po[$matiere]);
        }
        $this->setAProgrammesOfficiels($po);
    }

    private function buildQuestion($questionTypeID, $subject, array $res){
        if(in_array($questionTypeID, $this->getTypeQuestionsChosen())){
            $this->addQuestion([
                str_replace('XXX', $subject, $questionTypeID),
                $res
            ]);
        }
    }

    public function buildQuestionnaire(){

        $matieres = $this->getAProgrammesOfficiels();

        foreach ($matieres as $matiereName => $aMatiere) {

            $this->buildQuestion(self::TQ1, $matiereName, array_keys($this->getThemes($aMatiere)));

            foreach ($this->getThemes($aMatiere) as $themeName => $aTheme){
                if($themeName == "Langages et programmation" || $themeName == "Algorithmique" || $themeName == "Histoire de l’informatique"){
                    $this->buildQuestion(self::TQ2, $themeName, ['NSI (1ere)','NSI (Term)']);
                }else{
                    $this->buildQuestion(self::TQ2, $themeName, [$matiereName]);
                }

                $this->buildQuestion(self::TQ3, $themeName, array_keys($this->getContents($aTheme)));
                $this->buildQuestion(self::TQ7, "$themeName ( $matiereName )" , array_keys($this->getCapacitiesFromTheme($aTheme)));

                foreach ($this->getContents($aTheme) as $contentName => $aContent ){

                    $this->buildQuestion(self::TQ4, $contentName, [$matiereName]);
                    $this->buildQuestion(self::TQ5, $contentName, [$themeName]);
                    $this->buildQuestion(self::TQ6, $contentName, array_keys($this->getCapacitiesFromContent($aContent)));

                    foreach ($this->getCapacitiesFromContent($aContent) as $capacityName => $capacityComment){

                        $this->buildQuestion(self::TQ8, $capacityName, [$contentName]);
                        $this->buildQuestion(self::TQ9, $capacityName, ["$themeName ( $matiereName )"]);
                        if(!empty($capacityComment)) $this->buildQuestion(self::TQ10, $capacityName, [$capacityComment]);

                    }
                }
            }
        }
    }

    private function getCapacitiesFromTheme($aTheme){
        $tab = [];
        foreach ($this->getContents($aTheme) as $aContent){
            $tab = array_merge($tab, $this->getCapacitiesFromContent($aContent));
        }
        return $tab;
    }

    private function getCapacitiesFromContent($aContent){
        return $aContent["capacités attendues"];
    }

    private function getContents($aTheme){
        return $aTheme["contenus"];
    }

    /**
     * @return array
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * @param array $question
     */
    public function addQuestion($question)
    {
        $questions = $this->getQuestions();
        array_push($questions, $question);
        $this->questions = $questions;
    }

    /**
     * @return mixed
     */
    public function getAProgrammesOfficiels()
    {
        return $this->aProgrammesOfficiels;
    }

    /**
     * @param mixed $aProgrammesOfficiels
     */
    public function setAProgrammesOfficiels($aProgrammesOfficiels)
    {
        $this->aProgrammesOfficiels = $aProgrammesOfficiels;
    }

    /**
     * @return array
     */
    public function getTypeQuestions()
    {
        return [
            "Quels sont les themes de la matieres 'XXX' ?",
            "Dans quelle(s) matiere(s) se trouve le theme 'XXX' ?",
            "Quels sont les contenus du theme 'XXX' ?",
            "Dans quelle matiere se trouve le contenu 'XXX' ?",
            "Dans quel theme se trouve le contenu 'XXX' ?",
            "Quelle(s) est(sont) la(les) capacité(s) attendue(s) dans le contenu 'XXX' ?",
            "Quelle(s) est(sont) la(les) capacité(s) attendue(s) dans le theme 'XXX' ?",
            "A quel contenu appartient la capacité 'XXX' ?",
            "A quel theme appartient la capacité 'XXX' ?",
            "Quel commentaire est annoté a la capacité 'XXX' ?"
        ];
    }

    /**
     * @return array
     */
    public function getTypeQuestionsChosen()
    {
        return $this->typeQuestionsChosen;
    }

    /**
     * @param array $typeQuestionsChosen
     */
    public function setTypeQuestionsChosen($typeQuestionsChosen)
    {
        $this->typeQuestionsChosen = $typeQuestionsChosen;
    }

}